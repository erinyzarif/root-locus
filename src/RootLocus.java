import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Graphics;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JButton;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class RootLocus {

	private JFrame frame;
	private JPanel panel;
	private JPanel panel1;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					RootLocus window = new RootLocus();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public RootLocus() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 893, 591);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		panel = new JPanel();
		panel.setBounds(10, 51, 486, 410);
		frame.getContentPane().add(panel);
		
		
		JButton btnNewButton = new JButton("Show RootLocus");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				scaleX(); scaleY(); drawRoot();
			}
		});
		btnNewButton.setFont(new Font("Tahoma", Font.PLAIN, 18));
		btnNewButton.setBounds(142, 495, 181, 31);
		frame.getContentPane().add(btnNewButton);
		
		JButton btnShowSteps = new JButton("Show steps");
		btnShowSteps.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Result();
			}
		});
		btnShowSteps.setFont(new Font("Tahoma", Font.PLAIN, 18));
		btnShowSteps.setBounds(610, 495, 140, 31);
		frame.getContentPane().add(btnShowSteps);
		
		panel1 = new JPanel();
		panel1.setBounds(523, 10, 335, 475);
		frame.getContentPane().add(panel1);
		
	}
	
	private void scaleX() {
		Graphics g = panel.getGraphics();
		for(int i=0; i<500; i+=10) { 
			g.setColor(Color.lightGray); 
			g.drawLine(410-i, 0, 410-i, 500); 
			g.setColor(Color.black);
		}
		for(int i=0; i<500; i+=10) { 
			g.setColor(Color.lightGray); 			
			g.drawLine(410+i, 0, 410+i, 500); 
			g.setColor(Color.black);
		}
		g.drawLine(400, 20, 400, 390);
		g.drawLine(20, 20, 20, 390);
		g.drawLine(470, 20, 470, 390);
		g.drawString("0", 400, 406);
		for(int i=1; i<8; i++) {
			g.drawString(String.valueOf(-i*10) , 400-i*50-10, 406);
			g.setColor(Color.gray);
			g.drawLine(400-i*10*5, 20, 400-i*10*5, 390);
			g.setColor(Color.black);
		}
		g.setColor(Color.gray);
		g.drawLine(450, 20, 450, 390);
		g.setColor(Color.black);
		g.drawString("10", 445, 406);
	}
	
	private void scaleY() {
		Graphics g = panel.getGraphics();
		for(int i=0; i<400; i+=10) { 
			g.setColor(Color.lightGray); 
			g.drawLine(0, 210-i, 500, 210-i); 
			g.setColor(Color.black);
		}
		for(int i=0; i<400; i+=10) { 
			g.setColor(Color.lightGray); 
			g.drawLine(0, 210+i, 500, 210+i); 
			g.setColor(Color.black);		
		}
		g.drawLine(20, 200, 470, 200);
		g.drawLine(20, 20, 470, 20);
		g.drawLine(20, 390, 470, 390);
		g.drawString("0", 10, 205);
		for(int i=1; i<4; i++) {
			g.drawString(String.valueOf(-i*10), 0, 200+i*50+5);
			g.setColor(Color.gray);
			g.drawLine(20, 200+10*i*5, 470, 200+10*i*5);
			g.setColor(Color.black);
		}
		for(int i=1; i<4; i++) {
			g.drawString(String.valueOf(i*10), 5, 200-i*50+5);
			g.setColor(Color.gray);
			g.drawLine(20, 200-10*i*5, 470, 200-10*i*5);
			g.setColor(Color.black);
		}
	}
	
	private void drawRoot() {
		Graphics g = panel.getGraphics();
		g.setColor(Color.blue);
		g.drawLine(400, 200, 400-25*5, 200);
		g.setColor(Color.black);
		g.drawLine(403, 198, 398, 203);
		g.drawLine(398, 198, 403, 203);
		g.drawLine(273, 198, 278, 203);
		g.drawLine(278, 198, 273, 203);
		for(int i=0; i<165; i++) {
			double x1 = (0.01759690411)*Math.pow(0.5*i, 2)-9.150390137;
			double x2 = (0.01759690411)*Math.pow(0.5*i+1, 2)-9.150390137;
			g.drawLine(400+(int)x1-40, 200+i, 400+(int)x2-40, 200+i+1);
			
		}
		for(int i=0; i<165; i++) {
			double x1 = (0.01759690411)*Math.pow(0.5*i, 2)-9.150390137;
			double x2 = (0.01759690411)*Math.pow(0.5*i+1, 2)-9.150390137;
			g.drawLine(400+(int)x1-40, 200-i, 400+(int)x2-40, 200-i-1);
		}	
		g.drawLine(400-5*10*5, 200-10*5, 20, 20);
		g.drawLine(400-5*10*5, 200+10*5, 20, 390);
		g.drawLine(400-5*10*5+3, 200-10*5-3, 400-5*10*5-3, 200-10*5+3);
		g.drawLine(400-5*10*5-3, 200-10*5-3, 400-5*10*5+3, 400-5*10*5+3);
		g.drawLine(400-5*10*5+3, 200+10*5-3, 400-5*10*5-3, 200+10*5+3);
		g.drawLine(400-5*10*5-3, 200+10*5-3, 400-5*10*5+3, 200+5*10+3);

	}
	
	private void Result() {
		String[] poles = new String[4];
		int[] zeros = new int[0];
		poles[0]="0"; poles[0]="-25"; poles[0]="-50+10j"; poles[0]="-50-10j";
		
		int sump=0-125; int sumz=0;
		double asy = (sump-sumz)/(poles.length-zeros.length);
		
		int[] angle = new int[poles.length-zeros.length];
		for(int i=0; i<angle.length; i++) {
			angle[i] = ((2*i+1)*180)/(poles.length-zeros.length);
		}
		
		double breakaway = -9.150390137;
		int k = 2381600;
		double s = Math.sqrt(520);
		
		Graphics g = panel1.getGraphics();
		g.drawString("Poles:(0,-25,-50+10j,-50-10j)    ,and no zeroes", 10, 30);
		g.drawString("Number of poles:", 10, 50); g.drawString(String.valueOf(poles.length), 100, 50);
		g.drawString("Asympotetes:", 10, 70); g.drawString("o~ = ", 100, 70); g.drawLine( 130, 66, 285, 66);
		g.drawString("sum of poles - sum of zeroes", 130, 60); g.drawString("#poles - #zeroes", 160, 80); 
		g.drawString(" =", 290, 70);  g.drawString(String.valueOf(asy), 305, 70);
		g.drawString("theta =", 90, 110); g.drawLine(130, 105, 220, 105);
		g.drawString("(2*k+1)*180", 133, 100); g.drawString("#poles - #zeroes", 130, 120); 		
		g.drawString(" =", 220, 110); g.drawString(String.valueOf(angle[0]), 235, 110);
		g.drawString("at k=0   -->   theta =", 90, 140); g.drawString(String.valueOf(angle[0]), 200, 140);
		g.drawString("at k=1   -->   theta =", 90, 160); g.drawString(String.valueOf(angle[1]), 200, 160);
		g.drawString("at k=2   -->   theta =", 90, 180); g.drawString(String.valueOf(angle[2]), 200, 180);
		g.drawString("at k=3   -->   theta =", 90, 200); g.drawString(String.valueOf(angle[3]), 200, 200);
		g.drawString("G(s) = ", 10, 230); g.drawLine(50, 227, 200, 227); g.drawString("k", 120, 220);
		g.drawString("s(s+25)(s^2+100*s+2600)", 55, 240);
		g.drawString("k + s^4 + 125*s^3 + 5100*s^2 + 65000s = 0", 10, 260);
		g.drawString("dk/ds = -4*s^3 - 375*s^2 - 10200*s - 65000 ", 10, 280);
		g.drawString("after solving the differentiation equation ---->  s =", 10, 300);
		g.drawString(String.valueOf(breakaway), 270, 300);
		g.drawString("S^4", 10, 320); g.drawString("S^3", 10, 340); g.drawString("S^2", 10, 360);
		g.drawString("S^1", 10, 380); g.drawString("S^0", 10, 400);
		g.drawString("1", 60, 320); g.drawString("5100", 150, 320);
		g.drawString("125", 60, 340); g.drawString("65000", 150, 340);
		g.drawString("4580", 60, 360); g.drawString("k", 150, 360);		
		g.drawString("65000*4580-125*k", 40, 375); g.drawLine(35, 375, 138, 375);
		g.drawString("4580", 70, 388); g.drawString("k", 60, 400);
		g.drawString("after solving it  ---->  k = ", 10, 420); g.drawString(String.valueOf(k), 135, 420);
		g.drawString("---> 4580*s^2 + k = 0   (I)", 200, 360);
		g.drawString("after solving eqaution(I) to find instruction", 10, 440);
		g.drawString("---> s = -", 30, 460); g.drawString(String.valueOf(s), 75, 460);
		g.drawString(",s = ", 185, 460); g.drawString(String.valueOf(s), 210, 460);
	}
}
